# voting web application

Laravel + MySql + Node JS + Golang

-------------------------------------------------------------

## Commit prefix instructions :

#### 1- feat: The new feature you're adding

#### 2- fix: A bug fix

#### 3- style: Feature and updates related to styling

#### 4- refactor: Refactoring a specific section of the codebase

#### 5- test: Everything related to testing

#### 6- docs: Everything related to documentation

#### 7- chore: Regular code maintenance

example =>
feat: add login button

-------------------------------------------------------------

## execute laravel commands :

#### composer : docker-compose run --rm composer [COMMANDS] </br>
#### artisan : docker-compose run --rm artisan [COMMANDS] </br>
#### npm : docker-compose run --rm npm [COMMANDS] </br>
